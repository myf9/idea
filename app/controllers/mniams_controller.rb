class MniamsController < ApplicationController
  before_action :set_mniam, only: [:show, :destroy, :edit, :update, :upvote, :favorite]
  before_action :authenticate_user!, except: [:home]
  load_and_authorize_resource except: [:favorite, :upvote]

  def index
    @search = Mniam.ransack(params[:q])
    @mniams = @search.result.paginate(:page => params[:page], :per_page => 12)  
  end

  def show
    @comments = @mniam.comments.hash_tree
    @comment = @mniam.comments.build(parent_id: params[:parent_id])
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "show",:layout => 'pdf.html.erb',:template => '/mniams/show'  # Excluding ".pdf" extension.
      end
    end
  end

  def new
    @mniam = current_user.mniams.build
  end

  def edit
  end

  def create
    @mniam = current_user.mniams.build(mniam_params)
    if  @mniam.save 
      redirect_to @mniam, notice: "Eat was created"
    else 
      render 'new'
    end
  end

  def update
    if @mniam.update(mniam_params)
      @mniam.save
      redirect_to @mniam, notice: "Eat was updated"
    else
      render 'edit'
    end
  end

  def destroy
    @mniam.destroy
    redirect_to root_path, notice: "Eat was deleted"
  end

  def home
    if params[:search]
      @mniams = Mniam.search(params[:search]).paginate(:page => params[:page], :per_page => 12)  
    elsif params[:tag]
      @mniams = Mniam.tagged_with(params[:tag]).paginate(:page => params[:page], :per_page => 12)  
    elsif params[:con] && params[:type]
      @mniams = Mniam.filtering(params[:type], params[:con]).paginate(:page => params[:page], :per_page => 12)
    else
      @mniams = Mniam.all.paginate(:page => params[:page], :per_page => 12)  
    end
  end

  def favorite
    if params[:type] == "favorite"
      current_user.favorites << @mniam
      redirect_to mniams_favorite_list_path(current_user), notice: 'You favorited this mniams'
    else
      current_user.favorites.delete(@mniam)
      redirect_to :back, alert: 'Unfavorited this mnians'
    end
  end

  def favorite_list
    @mniams = current_user.favorites.uniq
  end

  def top
    @top = Mniam.sort_score[0..7]
  end

  def upvote 
    @mniam.upvote_by current_user
    redirect_to :back, notice: "You like this Mniams :)"
  end  

  private 

  def set_mniam
    @mniam = Mniam.friendly.find(params[:id])
  end

  def mniam_params
    params.require(:mniam).permit(:title, :description, :avatar, :tag_list, :slug, :price, :difficulty, :typ, :preparation_time,
                                 ingredients_attributes: [:id, :name, :_destroy], directions_attributes: [:id, :step, :_destroy])
  end
end
