class TipsController < ApplicationController
  require 'will_paginate/array' 
  load_and_authorize_resource except: [:upvote]
  before_action :find_tip, only: [:show, :upvote, :edit, :destroy, :update]
	before_action :authenticate_user!

  def index
    @search = Tip.ransack(params[:q])
    @tips = @search.result.order("created_at DESC").paginate(:page => params[:page], :per_page => 7)  
    if (params[:format])
      @tips = Tip.set_category(params[:format].to_s).paginate(:page => params[:page], :per_page => 7)  
    end
  end

  def show
    @comments = @tip.comments.hash_tree
    @comment = @tip.comments.build(parent_id: params[:parent_id])
  end

  def new
  	@tip = current_user.tips.build
  end 

  def create
  	@tip = current_user.tips.build(tip_params)
    @tip.movie = movie_conventer
    if @tip.save 
      redirect_to tips_path, notice: "Tip was created"
    else 
      render 'new'
    end
  end

  def edit
  end

  def update
    if @tip.update(tip_params)
      @tip.movie = movie_conventer
      @tip.save
      redirect_to @tip, notice: "Tip was updated"
    else
      render 'edit'
    end
  end

  def destroy
    @tip.destroy
    redirect_to tips_path, notice: "Tip was deleted"
  end

  def top
    @tips = Tip.the_best[0..4]
  end

  def movie_conventer
    yt_id = @tip.movie[@tip.movie.length-11...@tip.movie.length]
    yt_id.nil? ? yt = "" : yt = "https://www.youtube.com/embed/" + yt_id
  end

  def upvote 
    @tip.upvote_by current_user
    redirect_to :back, notice: "You like this Tip :)"
  end 

  private

    def tip_params
    	params.require(:tip).permit(:movie, :title, :description, :user_id, :category)
    end

    def find_tip
      @tip = Tip.find(params[:id])
    end
end
