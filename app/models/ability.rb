class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.nil?
        can :read, Mniam
    elsif user.admin?
        can :manage, :all
    else
        can :read, :all
        can :manage, Mniam, :user_id => user.id
        can :manage, Tip, :user_id => user.id
    end
  end
end