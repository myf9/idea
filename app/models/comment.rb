class Comment < ActiveRecord::Base
	acts_as_tree order: 'created_at DESC'
	belongs_to :user
	belongs_to :mniam
	belongs_to :tip

	VALID_REGEX = /\A^[\w \.\-@:),.!?"']*$\Z/
  	validates :body, presence: true, length: { in: 2..240}, format: { with: VALID_REGEX }
end
