class Direction < ActiveRecord::Base
  belongs_to :mniam

  VALID_REGEX = /\A^[\w \.\-@:),.!?"']*$\Z/
  validates :step, presence: true, length: { in: 2..240}, format: { with: VALID_REGEX }
  validates :mniam_id, presence: true
end
