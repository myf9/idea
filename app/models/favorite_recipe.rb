class FavoriteRecipe < ActiveRecord::Base
	belongs_to :mniam
  	belongs_to :user

  	validates :mniam_id, :user_id, presence: true
end
