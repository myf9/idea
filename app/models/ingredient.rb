class Ingredient < ActiveRecord::Base
  belongs_to :mniam

  VALID_REGEX = /\A^[\w \.\-@:),().!?"']*$\Z/
  validates :name, presence: true, length: { in: 2..90}, format: { with: VALID_REGEX }
  validates :mniam_id, presence: true
end
