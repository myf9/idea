class Message < ActiveRecord::Base
  belongs_to :conversation
  belongs_to :user

  validates_presence_of :body, :conversation_id, :user_id
  VALID_REGEX = /\A^[\w \.\-@:),().!?"']*$\Z/
  validates :body, length: { in: 1..190}, format: { with: VALID_REGEX }
end
