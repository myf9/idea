class Mniam < ActiveRecord::Base
	default_scope { order('created_at DESC') }
	belongs_to :user
	has_many :comments, dependent: :destroy
	has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "https://static.pexels.com/photos/2232/vegetables-italian-pizza-restaurant.jpg"
	validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
	has_many :ingredients, dependent: :destroy
	has_many :directions, dependent: :destroy

	has_many :favorite_recipes  
  has_many :favorited_by, through: :favorite_recipes, source: :mniam 

  accepts_nested_attributes_for :ingredients,
								  	reject_if: proc { |attributes| attributes['name'].blank? },
								  	allow_destroy: true
 	
 	accepts_nested_attributes_for :directions,
								 	reject_if: proc { |attributes| attributes['step'].blank? },
								  	allow_destroy: true

	acts_as_taggable_on :content, :name, :tag_list
	acts_as_taggable
	acts_as_votable
	extend FriendlyId
	friendly_id :title, use: :slugged

	VALID_REGEX = /\A^[\w \.\-@:),.!?"']*$\Z/
	#VALID_REGEX = /[a-zA-Z0-9\s]/	
	validates :title,  presence: true, length: { maximum: 50 }, format: { with: VALID_REGEX }
	validates :description,  presence: true, length: { maximum: 255 }, format: { with: VALID_REGEX }
	validates :price, length: { maximum: 3 }, :numericality => true, :allow_blank => true
	validates :preparation_time, length: { maximum: 3 }, :numericality => true, :allow_blank => true
	validates :difficulty, :typ, :user_id, presence: true

	def related(mniam)
		mniams = Mniam.all.select {|i| i.typ == mniam.typ and i.id != mniam.id}
		mniams = mniams[0..20]
	end

	def score
    self.get_upvotes.size 
  end

  def self.search(query)
    where("title like ?", "%#{query}%") 
  end

  def self.sort_score
    self.all.sort {|a, b| b.score<=>a.score}
  end

  def self.filtering(type, con)
  	if type.to_i != 0
  		self.all.select{|f| f if f[con] < type.to_i }
  	else
  		self.all.select{|f| f if f[con] == type.to_s }
  	end
  end
end
