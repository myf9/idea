class Tip < ActiveRecord::Base
	belongs_to :user
	has_many :comments, dependent: :destroy

	validates :user_id, :category, presence: true
	VALID_REGEX = /\A^[\w \.\-@:),.!?"']*$\Z/
	validates :title, presence: true, length: { in: 3..70 }, format: { with: VALID_REGEX }
	validates :description, presence: true, length: { in: 10..400 }, format: { with: VALID_REGEX }
	validates :movie, presence: true, length: { maximum: 50 }
	
	acts_as_votable

	def self.score
    self.get_upvotes.size 
  end

  def self.the_best
  	self.all.sort {|a, b| b.score<=>a.score}
  end

  def self.set_category(cat)
  	self.all.select {|a| a if a.category == cat}
  end
end
 