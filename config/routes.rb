Rails.application.routes.draw do
  root "mniams#home" 
  get 'top/tips' => 'tips#top', :as => :top_tips
  get 'static_pages/News'
  get 'static_pages/About'
  get 'static_pages/Contact'
  get 'mniams/home'
  get 'mniams/favorite_list' => 'mniams#favorite_list'
  get 'tags/:tag', to: 'mniams#home', as: :tag
  get '/download_pdf/:id(.:format)' => 'mniams#show', :method => :get, :as=>:show
  get 'feed' => 'users#feed', :as => :feed_list
  get 'top' => 'mniams#top', :as => :top
  get '/filtering/(:type)/(:con)' => 'mniams#home', :as => :filtering

  devise_for :users, controllers: { registrations: 'registrations' } do
    delete "/logout" => "devise/sessions#destroy", :as => :destroy_user_session
  end

  resources :mniams do
    put :favorite, on: :member
    member do
      put "like", to: "mniams#upvote"
    end
  end

  resources :contacts, only: [:new, :create]
  resources :conversations do
    resources :messages
  end

  resources :tips do
    member do
      put "like", to: "tips#upvote"
    end
  end

  resources :users, :only => [:show, :index, :destroy] do
    member do
      get :following, :followers
    end
  end

  resources :relationships, only: [:create, :destroy]
  resources :comments, only: [:create]
end
