

FactoryGirl.define do
  factory :mniam do
    title {Faker::Name.first_name }
    description {Faker::Lorem.sentence(3)}
    movie {Faker::Name.first_name }
    price 100
    difficulty "Hard"
    preparation_time 30
    typ "Dinner"
    user
  end
end

