
FactoryGirl.define do
  factory :tip do
    title {Faker::Name.first_name }
    description {Faker::Lorem.sentence(3)}
    movie {Faker::Name.first_name }
    category "Tip"
    user
  end
end

