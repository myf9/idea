require "rails_helper"

describe ApplicationHelper do
  describe "helper methods" do
	  context "#set_title_of_page Index" do
	    it { expect(helper.set_title_of_page("Index")).to eq("Index | Recipes") }
	  end

	  context "#set_title_of_page nil" do
	    it { expect(helper.set_title_of_page).to eq("Recipes") }
	  end
	end
end