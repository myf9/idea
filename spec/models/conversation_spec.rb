require 'rails_helper'

RSpec.describe Conversation, type: :model do
  describe "validations" do
  	it { should validate_uniqueness_of(:sender_id) }
  end

  describe "associations" do
  	it { should belong_to(:sender).class_name('User').with_foreign_key('sender_id') }
    it { should belong_to(:recipient).class_name('User').with_foreign_key('recipient_id') }
    it { should have_many(:messages).dependent(:destroy) }
  end
end 

