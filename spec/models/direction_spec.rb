require 'rails_helper'

RSpec.describe Direction, type: :model do

  describe "validations" do
  	# Basic validations
  	it { should validate_presence_of(:step) }
    it { should validate_presence_of(:mniam_id) }

  	# Inclusion/acceptance of values
  	it { should validate_length_of(:step).is_at_least(2).is_at_most(240) }

  	# Format validations
  	it { should_not allow_value("R<>%@?!").for(:step) }
  end

  describe "associations" do
    it { should belong_to(:mniam) }
  end
end 