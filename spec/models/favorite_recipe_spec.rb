require 'rails_helper'

RSpec.describe FavoriteRecipe, type: :model do

  describe "validations" do
  	# Basic validations
  	it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:mniam_id) }
  end

  describe "associations" do
    it { should belong_to(:mniam) }
    it { should belong_to(:user) }
  end
end 