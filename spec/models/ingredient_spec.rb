require 'rails_helper'

RSpec.describe Ingredient, type: :model do

  describe "validations" do
  	# Basic validations
  	it { should validate_presence_of(:name) }
    it { should validate_presence_of(:mniam_id) }

  	# Inclusion/acceptance of values
  	it { should validate_length_of(:name).is_at_least(2).is_at_most(90) }

  	# Format validations
  	it { should_not allow_value("R<>%@?!").for(:name) }
  end

  describe "associations" do
    it { should belong_to(:mniam) }
  end
end 