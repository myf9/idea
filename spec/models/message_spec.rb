require 'rails_helper'

RSpec.describe Message, type: :model do

  describe "validations" do
  	# Basic validations
  	it { should validate_presence_of(:body) }
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:conversation_id) }

  	# Inclusion/acceptance of values
  	it { should validate_length_of(:body).is_at_least(1).is_at_most(190) }

  	# Format validations
  	it { should_not allow_value("R<>%@?!").for(:body) }
  end

  describe "associations" do
    it { should belong_to(:user) }
    it { should belong_to(:conversation) }
  end
end 