#   def self.filtering(type, con)
#   	if type.to_i != 0
#   		self.all.select{|f| f if f[con] < type.to_i }
#   	else
#   		self.all.select{|f| f if f[con] == type.to_s }
#   	end
#   end

require 'rails_helper'

RSpec.describe Mniam, type: :model do
	it "has a valid mniam" do
    expect(build(:mniam)).to be_valid
  end

	let(:mniam) { create(:mniam) }

	describe "validations" do
		it { expect(mniam).to validate_presence_of(:title) }
  	it { expect(mniam).to validate_presence_of(:description) }
  	it { expect(mniam).to validate_presence_of(:difficulty) }
  	it { expect(mniam).to validate_presence_of(:typ) }
  	it { expect(mniam).to validate_presence_of(:user_id) }

  	it { expect(mniam).to validate_length_of(:title).is_at_most(50) }
  	it { expect(mniam).to validate_length_of(:description).is_at_most(255) }

  	invalid_regex = "R<>%@?!"
  	it { expect(mniam).to_not allow_value(invalid_regex).for(:title) }
  	it { expect(mniam).to_not allow_value(invalid_regex).for(:description) }
  	
  	it { expect(mniam).to validate_numericality_of(:price) }
  	it { expect(mniam).to validate_numericality_of(:preparation_time) }

  	it { expect(mniam).to allow_value("", nil).for(:price) }
  	it { expect(mniam).to allow_value("", nil).for(:preparation_time) }

		it { expect(mniam).to have_attached_file(:avatar) }
	  it { expect(mniam).to validate_attachment_content_type(:avatar).allowing('image/png', 'image/gif').rejecting('text/plain', 'text/xml') }
	end

	describe "associations" do
		it { expect(mniam).to belong_to(:user) }
		it { expect(mniam).to have_many(:comments).dependent(:destroy) }
		it { expect(mniam).to have_many(:directions).dependent(:destroy) }
		it { expect(mniam).to have_many(:ingredients).dependent(:destroy) }
		it { expect(mniam).to have_many(:favorite_recipes) }
		it { expect(mniam).to have_many(:favorited_by) }
		it { expect(mniam).to accept_nested_attributes_for(:ingredients) }
		it { expect(mniam).to accept_nested_attributes_for(:directions) }
  end

  describe "instance methods" do
		context "responds to its methods" do
  		it { expect(mniam).to respond_to(:related) }
  		it { expect(mniam).to respond_to(:score) }
  	end

  	context "executes methods correctly" do
  		context "#related" do
  			before do
	  			@mniam_first = 	create(:mniam, typ: "Dinner")
	  			@mniam_second = create(:mniam, typ: "Dinner")
	  			@mniam_third = 	create(:mniam, typ: "Lunch")
  			end

  			it "should has related mniam" do 
  				expect(@mniam_first.related(@mniam_first)).to include @mniam_second 
  			end

  			it "should not has the same mniam" do 
  				expect(@mniam_first.related(@mniam_first)).to_not include @mniam_first 
  			end

  			it "should not has other typ mniam" do 
  				expect(@mniam_first.related(@mniam_first)).to_not include @mniam_third 
  			end
  		end

  		it "#score" do
  			expect(mniam.score).to eq(0)
  		end
		end
	end

	describe "class methods" do
		context "responds to its methods" do
  		it { expect(Mniam).to respond_to(:search) }
  		it { expect(Mniam).to respond_to(:sort_score) }
  		it { expect(Mniam).to respond_to(:filtering) }
  	end

  	context "executes methods correctly" do
  		context ".search" do
  			it { expect(Mniam.search(mniam.title).last).to eq(Mniam.where("title like ?", mniam.title).last) }
  		end

  		context ".filtering" do
  			before do
	  			@mniam_first = 	create(:mniam, typ: "Dinner", price: 20)
	  			@mniam_second = create(:mniam, typ: "Dinner", price: 30)
	  			@mniam_third = 	create(:mniam, typ: "Lunch",  price: 5)
  			end

  			it "should has the same typ" do
  				expect(Mniam.filtering("Dinner", "typ")).to include @mniam_second 
  			end

  			it "should'nt has other typ" do
  			  expect(Mniam.filtering("Dinner", "typ")).to_not include @mniam_third 
  			end

  			it "should has mniam with lower and equals price" do
  				expect(Mniam.filtering(20, "price")).to include @mniam_third 
  			end

  			it "should'nt has mniam with greater price" do
  				expect(Mniam.filtering(20, "price")).to_not include @mniam_second
  			end
  		end
  	end
	end
end
