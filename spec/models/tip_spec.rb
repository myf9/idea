require 'rails_helper'

RSpec.describe Tip, type: :model do

	it "has a valid tip" do
    expect(build(:tip)).to be_valid
  end

  let(:tip) { build(:tip) }
  
  describe "validations" do
  	# Basic validations
  	it { expect(tip).to validate_presence_of(:user_id) }
    it { expect(tip).to validate_presence_of(:category) }
    it { expect(tip).to validate_presence_of(:title) }
    it { expect(tip).to validate_presence_of(:description) }
    it { expect(tip).to validate_presence_of(:movie) }

  	# Inclusion/acceptance of values
  	it { expect(tip).to validate_length_of(:title).is_at_least(3).is_at_most(70) }
    it { expect(tip).to validate_length_of(:description).is_at_least(10).is_at_most(400) }
    it { expect(tip).to validate_length_of(:movie).is_at_most(50) }

  	# Format validations
  	it { expect(tip).to_not allow_value("R<>%@?!").for(:title) }
    it { expect(tip).to_not allow_value("R<>%@?!").for(:description) }
  end

  describe "associations" do
  	it { expect(tip).to belong_to(:user) }
    it { expect(tip).to have_many(:comments).dependent(:destroy) }
  end

	describe "class methods" do
    context "responds to its methods" do
      it { expect(Tip).to respond_to(:score) }
      it { expect(Tip).to respond_to(:the_best) }
      it { expect(Tip).to respond_to(:set_category) }
    end
		
    context "executes methods correctly" do
      context ".set_category" do
        before do
          @tip_one = create(:tip, category: "Tip")
          @tip_two = create(:tip, category: "Dinner")
        end

        it { expect(Tip.set_category("Tip")).to include @tip_one }
        it { expect(Tip.set_category("Tip")).to_not include @tip_two }
      end
    end
	end
end 
