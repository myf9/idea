require 'rails_helper'

RSpec.describe User, type: :model do

	it "has a valid order" do
    expect(build(:user)).to be_valid
  end

	let(:user) { create(:user) }

	describe "validations" do
		it { expect(user).to have_attached_file(:user_avatar) }
	  it { expect(user).to validate_attachment_content_type(:user_avatar).allowing('image/png', 'image/gif').rejecting('text/plain', 'text/xml') }
	end

	describe "associations" do
  	it { expect(user).to have_many(:mniams).dependent(:destroy) }
  	it { expect(user).to have_many(:comments).dependent(:destroy) }
  	it { expect(user).to have_many(:tips).dependent(:destroy) }
  	it { expect(user).to have_many(:favorite_recipes)}
  	it { expect(user).to have_many(:favorites) }
  	it { expect(user).to have_many(:following) }
  	it { expect(user).to have_many(:followers) }
  	it { expect(user).to have_many(:active_relationships).class_name('Relationship').with_foreign_key('follower_id').dependent(:destroy)  }
  	it { expect(user).to have_many(:passive_relationships).class_name('Relationship').with_foreign_key('followed_id').dependent(:destroy)  }
  	it { expect(user).to have_many(:conversations).with_foreign_key('sender_id').dependent(:destroy) }
  end

	describe "class methods" do
		context "responds to its methods" do
  		it { expect(User).to respond_to(:sort_score) }
  	end
	end

	describe "instance methods" do
		context "responds to its methods" do
  		it { expect(user).to respond_to(:follow) }
  		it { expect(user).to respond_to(:feed) }
  		it { expect(user).to respond_to(:unfollow) }
  		it { expect(user).to respond_to(:following?) }
  		it { expect(user).to respond_to(:scores) }
  	end

  	context "executes methods correctly" do
  		before do
  			@user_first = create(:user)
  			@user_second = create(:user)
  		end

  		it "#follow in #following?" do
  			@user_first.follow(@user_second)
  			expect(@user_first.following?(@user_second)).to eq(true)
  		end

  		it "#feed" do
  			relation = create(:relationship, follower_id: @user_first.id, followed_id: @user_second.id)
  			mniam = create(:mniam, user: @user_second)
  			expect(@user_first.feed).to include mniam
  		end

  		it "#unfollow in #following?" do
  			relation = create(:relationship, follower_id: @user_first.id, followed_id: @user_second.id)
  			@user_first.unfollow(@user_second)
  			expect(@user_first.following?(@user_second)).to eq(false)
  		end

  		it "#scores" do
  			expect(user.scores(user)).to eq(0) 
  		end
		end
	end
end