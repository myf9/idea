require "rails_helper"

RSpec.describe "Contacts", type: :routing do
  describe "CRUD's route" do
    it "to #new" do
      expect(get("/contacts/new")).to route_to("contacts#new")
    end

    it "to #create" do
      expect(post("/contacts")).to route_to("contacts#create")
    end
  end
end