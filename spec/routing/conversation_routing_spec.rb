require "rails_helper"

RSpec.describe "Conversations", type: :routing do
  describe "CRUD's route" do
    it "to #index" do
      expect(get("/conversations")).to route_to("conversations#index")
    end

    it "to #new" do
      expect(get("/conversations/new")).to route_to("conversations#new")
    end

    it "to #show" do
      expect(get("/conversations/1")).to route_to("conversations#show", :id => "1")
    end

    it "to #edit" do
      expect(get("/conversations/1/edit")).to route_to("conversations#edit", :id => "1")
    end

    it "to #create" do
      expect(post("/conversations")).to route_to("conversations#create")
    end

    it "to #update" do
      expect(put("/conversations/1")).to route_to("conversations#update", :id => "1")
    end

    it "to #destroy" do
      expect(delete("/conversations/1")).to route_to("conversations#destroy", :id => "1")
    end
  end

  describe "messages CRUD's route" do
    it "to #index" do
      expect(get("/conversations/1/messages")).to route_to("messages#index", :conversation_id => "1")
    end

    it "to #new" do
      expect(get("/conversations/1/messages/new")).to route_to("messages#new", :conversation_id => "1")
    end

    it "to #show" do
      expect(get("/conversations/1/messages/1")).to route_to("messages#show", :conversation_id => "1", :id => "1")
    end

    it "to #edit" do
      expect(get("/conversations/1/messages/1/edit")).to route_to("messages#edit", :conversation_id  => "1", :id => "1")
    end

    it "to #create" do
      expect(post("/conversations/1/messages")).to route_to("messages#create", :conversation_id => "1")
    end

    it "to #update" do
      expect(put("/conversations/1/messages/1")).to route_to("messages#update", :conversation_id  => "1", :id => "1")
    end

    it "to #destroy" do
      expect(delete("/conversations/1/messages/1")).to route_to("messages#destroy", :conversation_id  => "1", :id => "1")
    end
  end
end