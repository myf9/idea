require "rails_helper"

RSpec.describe "Mniams", type: :routing do
  describe "CRUD's route" do
    it "to #index" do
      expect(get("/mniams")).to route_to("mniams#index")
    end

    it "to #new" do
      expect(get("/mniams/new")).to route_to("mniams#new")
    end

    it "to #show" do
      expect(get("/mniams/1")).to route_to("mniams#show", :id => "1")
    end

    it "to #edit" do
      expect(get("/mniams/1/edit")).to route_to("mniams#edit", :id => "1")
    end

    it "to #create" do
      expect(post("/mniams")).to route_to("mniams#create")
    end

    it "to #update" do
      expect(put("/mniams/1")).to route_to("mniams#update", :id => "1")
    end

    it "to #destroy" do
      expect(delete("/mniams/1")).to route_to("mniams#destroy", :id => "1")
    end
  end

  describe "members route" do
	  it "to #like" do
	  	expect(put("/mniams/1/like")).to route_to("mniams#upvote", :id => "1")
	  end

    it "to #favorite" do
      expect(put("/mniams/1/favorite")).to route_to("mniams#favorite", :id => "1")
    end
	end

  describe "others route" do
    it "to #root" do
      expect(get("/")).to route_to("mniams#home")
    end

    it "to #home" do
      expect(get("/mniams/home")).to route_to("mniams#home")
    end

    it "to #favorite_list" do
      expect(get("/mniams/favorite_list")).to route_to("mniams#favorite_list")
    end

    it "to #tag" do
      expect(get("/tags/Lolo")).to route_to("mniams#home", :tag => "Lolo")
    end

    it "to #pdf" do
      expect(get("/download_pdf/1")).to route_to("mniams#show", :id => "1", :method => :get)
    end

    it "to #top" do
      expect(get("/top")).to route_to("mniams#top")
    end

    it "to #filtering" do
      expect(get("/filtering/Type/Con")).to route_to("mniams#home", :type => "Type", :con => "Con")
    end
  end
end