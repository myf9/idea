require "rails_helper"

RSpec.describe "Relationships", type: :routing do
  describe "CRUD's route" do
    it "to #create" do
      expect(post("/relationships")).to route_to("relationships#create")
    end

    it "to #destroy" do
      expect(delete("/relationships/1")).to route_to("relationships#destroy", :id => "1")
    end
  end
end