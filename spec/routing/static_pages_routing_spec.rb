require "rails_helper"

RSpec.describe "StaticPages", type: :routing do
  describe "others route" do
    it "to #News" do
      expect(get("/static_pages/News")).to route_to("static_pages#News")
    end

    it "to #About" do
      expect(get("/static_pages/About")).to route_to("static_pages#About")
    end

    it "to #Contact" do
      expect(get("/static_pages/Contact")).to route_to("static_pages#Contact")
    end
  end
end