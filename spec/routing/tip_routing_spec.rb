require "rails_helper"

RSpec.describe "Tips", type: :routing do
  describe "CRUD's route" do
    it "to #index" do
      expect(get("/tips")).to route_to("tips#index")
    end

    it "to #new" do
      expect(get("/tips/new")).to route_to("tips#new")
    end

    it "to #show" do
      expect(get("/tips/1")).to route_to("tips#show", :id => "1")
    end

    it "to #edit" do
      expect(get("/tips/1/edit")).to route_to("tips#edit", :id => "1")
    end

    it "to #create" do
      expect(post("/tips")).to route_to("tips#create")
    end

    it "to #update" do
      expect(put("/tips/1")).to route_to("tips#update", :id => "1")
    end

    it "to #destroy" do
      expect(delete("/tips/1")).to route_to("tips#destroy", :id => "1")
    end
  end

  describe "members route" do
	  it "to #like" do
	  	expect(put("/tips/1/like")).to route_to("tips#upvote", :id => "1")
	  end
	end

  describe "others route" do
    it "to #top" do
      expect(get("top/tips")).to route_to("tips#top")
    end
  end
end