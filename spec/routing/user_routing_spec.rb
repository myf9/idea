require "rails_helper"

RSpec.describe "Users", type: :routing do
  describe "CRUD's route" do
    it "to #index" do
      expect(get("/users")).to route_to("users#index")
    end

    it "to #show" do
      expect(get("/users/1")).to route_to("users#show", :id => "1")
    end

    it "to #destroy" do
      expect(delete("/users/1")).to route_to("users#destroy", :id => "1")
    end
  end

  describe "members route" do
	  it "to #following" do
	  	expect(get("/users/1/following")).to route_to("users#following", :id => "1")
	  end
    it "to #followers" do
      expect(get("/users/1/followers")).to route_to("users#followers", :id => "1")
    end
	end

  describe "others route" do
    it "to #feed" do
      expect(get("/feed")).to route_to("users#feed")
    end
  end
end